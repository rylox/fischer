//
// Created by genkinger on 27.06.18.
//

#include "Tag.h"

namespace NFC {
    Tag::Tag(FreefareTag freefareTag) : mFreefareTag(freefareTag) {

    }

    void Tag::WriteMessage(NFC::NFCMessage message, bool tlv) {
        std::vector<uint8_t> msg;
        if (tlv) {
            msg = message.GetMessageTLV();
        } else {
            msg = message.GetMessage();
        }

        WriteRaw(msg);
    }

    NFCMessage Tag::ReadMessage() {
        auto read = ReadPagesFromTable(0, static_cast<uint32_t>(pageTable.size() - 1));
        uint16_t size = 0;
        uint8_t type = 0;
        uint8_t *decoded = tlv_decode(read.data(), &type, &size);

        if (decoded == nullptr || size == 0) {
            LOG("Failed to decode TLV message...");
            return NFCMessage();
        }

        NFCMessage message;

        std::vector<uint8_t> decodedString;
        decodedString.assign(decoded, decoded + size);
        switch (decodedString[3]) {
            case TEXT_INDICATOR_BYTE:
                LOG("READ TEXT");
                message.SetMessage(decodedString, NFCMessageFormat::TEXT);
                message.SetFormat(NFCMessageFormat::TEXT);
                break;
            case LINK_INDICATOR_BYTE:
                LOG("READ LINK");
                message.SetMessage(decodedString, NFCMessageFormat::LINK);
                message.SetFormat(NFCMessageFormat::LINK);
                break;
            default:
                LOG("Invalid indicator byte...");
                return NFCMessage();
        }

        return message;
    }

    void Tag::WritePage(uint8_t page, uint32_t value) {
        ntag21x_write(mFreefareTag, page, reinterpret_cast<uint8_t *>(&value));
    }

    uint32_t Tag::ReadPage(uint8_t page) {
        uint32_t ret = 0;
        ntag21x_fast_read4(mFreefareTag, page, reinterpret_cast<uint8_t *>(&ret));
        return ret;
    }

    std::vector<uint8_t> Tag::ReadPages(uint8_t startPage, uint8_t endPage) {
        if (startPage >= endPage || !InPageTable(startPage) || !InPageTable(endPage)) {
            LOG("Invalid page numbers...");
            return std::vector<uint8_t>();
        }

        std::vector<uint8_t> output(static_cast<unsigned long>(endPage - startPage + 1) * 4);
        ntag21x_fast_read(mFreefareTag, startPage, endPage, &output[0]);
        return output;
    }

    std::vector<uint8_t> Tag::ReadPagesFromTable(uint32_t startPageIndex, uint32_t endPageIndex) {
        auto pageTableMaxIndex = pageTable.size() - 1;
        if (startPageIndex > pageTableMaxIndex || endPageIndex > pageTableMaxIndex) {
            LOG("Index out of bounds...");
        }
        return ReadPages(pageTable[startPageIndex], pageTable[endPageIndex]);
    }

    void Tag::WriteRaw(std::vector<uint8_t> data, uint32_t pageOffset) {
        auto size = data.size();
        if (size > ((pageTable.size() - pageOffset) * 4) || size == 0) {
            LOG("Invalid size...");
            return;
        }

        auto rem = size % 4;

        unsigned long padding = 0;
        if (rem != 0) {
            padding = 4 - rem;
        }

        auto paddedSize = (size + padding);
        auto paddedBuffer = std::vector<uint8_t>(std::begin(data), std::end(data));
        for (int i = 0; i < padding; i++) {
            paddedBuffer.push_back(0);
        }

        auto dataPtr = reinterpret_cast<uint32_t *>(paddedBuffer.data());
        auto inter = std::vector<uint32_t>(dataPtr, dataPtr + (paddedSize / 4));
        int index = 0;

        for (auto d : inter) {
            WritePage(pageTable[index + pageOffset], d);
            index++;
        }
    }

    void Tag::Clear() {
        auto pages = ReadPagesFromTable(0, static_cast<uint8_t>(pageTable.size()-1));
        auto ptr = reinterpret_cast<uint32_t*>(pages.data());
        auto pageValues = std::vector<uint32_t>(ptr,ptr+pages.size()/4);

        uint8_t index = 0;
        for (auto value : pageValues) {
            if (value != 0) {
                WritePage(index, 0);
                LOG("CLEARING...");
            }
            index++;
        }
    }

    void Tag::Open() {
        if (ntag21x_connect(mFreefareTag) < 0) {
            LOG("Failed to connect to tag...");
        }
        if (ntag21x_get_info(mFreefareTag) < 0) {
            LOG("Failed to get info...");
        }
    }

    FreefareTag Tag::GetFreefareTag() {
        return mFreefareTag;
    }

    bool Tag::InPageTable(uint8_t page) {
        for (uint8_t p: pageTable) {
            if (page == p) {
                return true;
            }
        }
        return false;
    }

    std::vector<uint8_t> Tag::GetUID() {
        char *uid = freefare_get_tag_uid(mFreefareTag);
        return std::vector<uint8_t>(uid, uid + strlen(uid));
    }

    Tag::~Tag() {
        freefare_free_tag(mFreefareTag);
    }

}
