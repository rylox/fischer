//
// Created by genkinger on 15/06/18.
//

#include "NFCMessage.h"

namespace NFC {

    NFCMessage::NFCMessage() : mFormat(NFCMessageFormat::RAW) {}

    NFCMessage::NFCMessage(std::string message, NFCMessageFormat format) : mFormat(NFCMessageFormat::RAW) {
        SetMessage(message, format);
    }

    NFCMessage::NFCMessage(std::vector<uint8_t> message, NFCMessageFormat format) : mFormat(NFCMessageFormat::RAW){
        SetMessage(message, format);
    }

    void NFCMessage::SetFormat(NFC::NFCMessageFormat format) {
        this->mMessage = formatMessage(this->mMessage, this->mFormat, format);
        this->mFormat = format;
    }

    void NFCMessage::SetMessage(std::vector<uint8_t> message, NFCMessageFormat format) {
        this->mMessage = formatMessage(message, format, this->mFormat);
    }

    void NFCMessage::SetMessage(std::string message, NFCMessageFormat format) {
        SetMessage(StringToVector(message), format);
    }

    std::vector<uint8_t>
    NFCMessage::formatMessage(std::vector<uint8_t> message, NFCMessageFormat from, NFCMessageFormat to) {
        std::vector<uint8_t> rawMessage;
        switch (from) {
            case NFCMessageFormat::RAW:
                rawMessage = message;
                break;
            case NFCMessageFormat::TEXT:
                rawMessage = TextToRaw(message);
                break;
            case NFCMessageFormat::LINK:
                rawMessage = LinkToRaw(message);
                break;
            case NFCMessageFormat::MACHINE:
                rawMessage = MachineToRaw(message);
                break;
        }

        std::vector<uint8_t> output;
        switch (to) {
            case NFCMessageFormat::RAW:
                output = rawMessage;
                break;
            case NFCMessageFormat::TEXT:
                output = RawToText(message);
                break;
            case NFCMessageFormat::LINK:
                output = RawToLink(message);
                break;
            case NFCMessageFormat::MACHINE:
                output = RawToMachine(message);
                break;
        }

        return output;
    }

    std::vector<uint8_t> NFCMessage::TextToRaw(std::vector<uint8_t> message) {
        return std::vector<uint8_t>(std::begin(message) + 7, std::end(message));
    }

    std::vector<uint8_t> NFCMessage::LinkToRaw(std::vector<uint8_t> message) {
        return std::vector<uint8_t>(std::begin(message) + 5, std::end(message)-1);
    }

    std::vector<uint8_t> NFCMessage::MachineToRaw(std::vector<uint8_t> message) {
        return std::vector<uint8_t>(std::begin(message) + 1, std::end(message));
    }


    std::vector<uint8_t> NFCMessage::RawToText(std::vector<uint8_t> message) {
        std::vector<uint8_t> output;

        output.push_back(0xd1);
        output.push_back(0x01);
        output.push_back((uint8_t) (message.size() + 3));
        output.push_back(TEXT_INDICATOR_BYTE);
        output.push_back(0x02);
        output.push_back('e');
        output.push_back('n');
        output.insert(std::end(output), std::begin(message), std::end(message));
        return output;
    }

    std::vector<uint8_t> NFCMessage::RawToLink(std::vector<uint8_t> message) {
        std::vector<uint8_t> output;

        output.push_back(0xd1);
        output.push_back(0x01);
        output.push_back((uint8_t) (message.size() + 2));
        output.push_back(LINK_INDICATOR_BYTE);
        output.push_back(0x04);
        output.insert(std::end(output), std::begin(message), std::end(message));
        output.push_back(0x20);

        return output;
    }

    std::vector<uint8_t> NFCMessage::RawToMachine(std::vector<uint8_t> message) {
        if (message.size() != 3) {
            LOG("invalid mMessage length...");
            return {'$', 0, 0, 0};// dodgy af
        }

        auto output = std::vector<uint8_t>();
        output.push_back('$');
        output.insert(std::end(output), std::begin(message), std::end(message));
        return output;
    }

    NFCMessageFormat NFCMessage::GetFormat() {
        return mFormat;
    }

    std::vector<uint8_t> NFCMessage::GetMessage() {
        return mMessage;
    }

    std::vector<uint8_t> NFCMessage::GetMessageTLV() {
        return WrapTLV(mMessage);
    }

    std::vector<uint8_t> NFCMessage::WrapTLV(std::vector<uint8_t> message) {
        size_t size = 0;
        uint8_t *tlv = tlv_encode(3, message.data(), message.size(), &size);
        return std::vector<uint8_t>(tlv, tlv + size);
    }

    void NFCMessage::AppendToMessage(std::string message) {
        AppendToMessage(StringToVector(message));
    }

    void NFCMessage::AppendToMessage(std::vector<uint8_t> message) {
        std::vector<uint8_t> old = formatMessage(mMessage,mFormat,NFCMessageFormat::RAW);
        old.insert(std::end(old),std::begin(message),std::end(message));
        mMessage = formatMessage(old,NFCMessageFormat::RAW,mFormat);
    }

    std::vector<uint8_t> NFCMessage::StringToVector(std::string str) {
        std::vector<uint8_t> output;
        output.reserve(str.length());
        for (char c : str) {
            output.push_back(static_cast<uint8_t >(c));
        }
        return output;
    }

    std::vector<uint8_t> NFCMessage::GetMessage(NFCMessageFormat format) {
        return formatMessage(mMessage,mFormat,format);
    }


}
