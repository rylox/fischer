//
// Created by Rylox on 25.06.2018.
//

#include <nfc/nfc-types.h>
#include "NFCModule.h"

namespace NFC {

    NFCModule::NFCModule() {
        nfc_init(&mContext);
        if (!mContext) {
            ERR("Failed to initialize NFC", -1);
        }

        mCurrentDevice = nfc_open(mContext, nullptr);
        if (!mCurrentDevice) {
            ERR("No valid device found...",-1);
        }
    }

    NFCModule::~NFCModule() {
        if (mCurrentDevice) {
            nfc_close(mCurrentDevice);
        }
        nfc_exit(mContext);
    }

    Tag NFCModule::Next() {
        nfc_modulation nm = {.nmt = NMT_ISO14443A, .nbr = NBR_106};
        while (nfc_initiator_select_passive_target(mCurrentDevice, nm, nullptr, 0, &mTarget) < 0);
        return Tag(freefare_tag_new(mCurrentDevice, mTarget));
    }

    std::vector<uint8_t> NFCModule::NextUID() {
        nfc_modulation nm = {.nmt = NMT_ISO14443A, .nbr = NBR_106};
        while (nfc_initiator_select_passive_target(mCurrentDevice, nm, nullptr, 0, &mTarget) < 0);
        return std::vector<uint8_t>(std::begin(mTarget.nti.nai.abtUid), std::end(mTarget.nti.nai.abtUid));
    }

    void NFCModule::Poll() {
        nfc_modulation nm = {.nmt = NMT_ISO14443A, .nbr = NBR_106};
        while (nfc_initiator_select_passive_target(mCurrentDevice, nm, nullptr, 0, nullptr) < 0);
    } //Experimental

}