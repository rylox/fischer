//
// Created by genkinger on 27.06.18.
//

#ifndef FISCHER_NFCMESSAGEFORMAT_H
#define FISCHER_NFCMESSAGEFORMAT_H

namespace NFC{
    enum class NFCMessageFormat{
        TEXT,
        LINK,
        MACHINE,
        RAW
    };
}

#endif //FISCHER_NFCMESSAGEFORMAT_H
