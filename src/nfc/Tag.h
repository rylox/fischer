//
// Created by genkinger on 27.06.18.
//

#ifndef FISCHER_TAG_H
#define FISCHER_TAG_H

#include <freefare.h>
#include "NFCMessage.h"
#include <vector>

namespace NFC {

    class Tag {
    private:
        const std::vector<uint8_t> pageTable = {0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b,
                                                0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13,
                                                0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b,
                                                0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23,
                                                0x24, 0x25, 0x26, 0x27};/* last one reserved for machine instructions */

    public:

        Tag(FreefareTag freefareTag);

        ~Tag();

        void WriteMessage(NFCMessage message, bool tlv = true);

        NFCMessage ReadMessage();

        void WritePage(uint8_t page, uint32_t value);

        uint32_t ReadPage(uint8_t page);

        std::vector<uint8_t> ReadPages(uint8_t startPage, uint8_t endPage);

        std::vector<uint8_t> ReadPagesFromTable(uint32_t startPageIndex, uint32_t endPageIndex);

        void WriteRaw(std::vector<uint8_t>, uint32_t pageOffset = 0);

        void Open();

        void Clear();

        FreefareTag GetFreefareTag();

        std::vector<uint8_t> GetUID();

    private:

        bool InPageTable(uint8_t page);

    private:
        FreefareTag mFreefareTag;

    };
}

#endif //FISCHER_TAG_H
