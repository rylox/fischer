//
// Created by genkinger on 15/06/18.
//

#ifndef FISCHER_NFCMESSAGE_H
#define FISCHER_NFCMESSAGE_H

#include "../common.h"
#include "NFCMessageFormat.h"
#include <freefare.h>
#include <vector>

#define LINK_INDICATOR_BYTE 0x55
#define TEXT_INDICATOR_BYTE 0x54

namespace NFC {
    class NFCMessage {
        /*
        public:
        static const uint8_t LINK_INDICATOR_BYTE = 0x55;
        static const uint8_t TEXT_INDICATOR_BYTE = 0x54;
        */
    public:
        NFCMessage();
        NFCMessage(std::string message, NFCMessageFormat format = NFCMessageFormat::RAW);
        NFCMessage(std::vector<uint8_t> message, NFCMessageFormat format = NFCMessageFormat::RAW);
        void SetFormat(NFCMessageFormat format);
        void SetMessage(std::vector<uint8_t> message, NFCMessageFormat format = NFCMessageFormat::RAW);
        void SetMessage(std::string message, NFCMessageFormat format = NFCMessageFormat::RAW);

        void AppendToMessage(std::string message);
        void AppendToMessage(std::vector<uint8_t> message);

        NFCMessageFormat GetFormat();
        std::vector<uint8_t> GetMessage();
        std::vector<uint8_t> GetMessage(NFCMessageFormat format);
        std::vector<uint8_t> GetMessageTLV();

    private:
        std::vector<uint8_t> formatMessage(std::vector<uint8_t> message, NFCMessageFormat from, NFCMessageFormat to);
        std::vector<uint8_t> TextToRaw(std::vector<uint8_t> message);
        std::vector<uint8_t> LinkToRaw(std::vector<uint8_t> message);
        std::vector<uint8_t> MachineToRaw(std::vector<uint8_t> message);
        std::vector<uint8_t> RawToText(std::vector<uint8_t> message);
        std::vector<uint8_t> RawToLink(std::vector<uint8_t> message);
        std::vector<uint8_t> RawToMachine(std::vector<uint8_t> message);
        std::vector<uint8_t> WrapTLV(std::vector<uint8_t> message);

        std::vector<uint8_t> StringToVector(std::string str);

    private:
        std::vector<uint8_t> mMessage;
        NFCMessageFormat mFormat;
    };
}


#endif //FISCHER_NFCMESSAGE_H