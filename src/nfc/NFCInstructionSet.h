//
// Created by genkinger on 29/06/18.
//

#ifndef FISCHER_NFCINSTRUCTIONSET_H
#define FISCHER_NFCINSTRUCTIONSET_H

#include "NFCMessage.h"
#include <map>

namespace NFC {
    struct NFCInstructionSet {
        NFCMessage All;

        NFCMessage Blue;
        NFCMessage Red;
        NFCMessage White;

        std::map<std::vector<uint8_t>,NFCMessage> UID;
    };
}

#endif //FISCHER_NFCINSTRUCTIONSET_H
