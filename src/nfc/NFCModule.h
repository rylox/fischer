//
// Created by Rylox on 25.06.2018.
//

#ifndef FISCHER_NFCMOD_H
#define FISCHER_NFCMOD_H

#include "nfc/nfc.h"
#include "freefare.h"
#include "NFCMessage.h"
#include "Tag.h"
#include "../common.h"

namespace NFC {
    class NFCModule {
    public:
        NFCModule();
        Tag Next();
        virtual ~NFCModule();
        std::vector<uint8_t> NextUID();
        void Poll();
    private:

        nfc_context *mContext;
        nfc_device *mCurrentDevice;
        nfc_target mTarget;
    };
}

#endif //FISCHER_NFCMOD_H
