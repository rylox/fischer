#include "common.h"

void PrintHex(const uint8_t *pbtData, const size_t szBytes) {
    if (!pbtData) {
        LOG("Empty input to PrintHex...");
        return;
    }


    for (size_t szPos = 0; szPos < szBytes; szPos++) {
        printf("%02x  ", pbtData[szPos]);
    }
    printf("\n");
}
void PrintAscii(const char *pbtData, const size_t szBytes) {
    {
        if (!pbtData) {
            LOG("Empty input to PrintAscii...");
            return;
        }

        for (size_t szPos = 0; szPos < szBytes; szPos++) {
            printf("%c", pbtData[szPos]);
        }
        printf("\n");
    }
}
std::string timestr(time_t t) {
    std::stringstream strm;
    strm << t;
    return strm.str();
}
