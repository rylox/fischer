//
// Created by Rylox on 01.07.2018.
//

#include <atomic>
#include <iomanip>
#include "MAINTest.h"

enum class TagColor {
    BLUE,
    RED,
    WHITE
};

struct InventoryEntry {
    std::string timestamp;
    std::vector<uint8_t> uid;
    std::string color;
};


NFC::Tag tag(nullptr);
std::string tagcolor;

class MAINTest {

public:
    MAINTest() = default;;

    void Start(const std::string &connectAddress) {
        NFC::NFCModule nfcModule;
        MQTT::MQTTModule mqttModule(connectAddress, "peter");
        InstructionParser parser{};

        parser.RegisterWriteFunction(NFCWrite);
        parser.RegisterAppendFunction(NFCAppend);
        parser.RegisterStartFunction(StartProg);
        parser.RegisterStopFunction(StopProg);

        mqttModule.Connect();

        std::atomic<bool> opts = true;
        std::atomic<bool> quit = false;

        std::deque<std::string> instrQueue;
        mqtt::const_message_ptr recv;

        std::vector<InventoryEntry> inv;

        while(true) {
            auto msg = mqttModule.GetClient().consume_message();
            if(msg->get_topic().compare("sensors/status/run/" + mqttModule.GetClient().get_client_id())){
                if(!msg->get_payload().empty() && msg->get_payload().compare("1")){
                    break;
                }
            }
        }

        int color = 0;
        while (true) {
            std::thread t1() {
                nfcModule.Poll();
                opts = false;
            };

            while (opts) {
                if (quit) {
                    break;
                }
                if (mqttModule.GetClient().try_consume_message(&recv)) {
                    if (recv->get_topic() == "sensors/status/instructions/" + mqttModule.GetClient().get_client_id()) {
                        if (!recv->get_payload().empty()) {
                            instrQueue.push_back(recv->get_payload());
                        }

                    } else if (recv->get_topic() == "sensors/status/run/" + mqttModule.GetClient().get_client_id()) {
                        if (!recv->get_payload().empty()) {
                            if (recv->get_payload().compare("0")) {
                                quit = 1;
                            }
                        }
                    }
                }
            }

            if (quit) {
                break;
            }

            tag = nfcModule.Next();

            switch (color) {
                case 0:
                    tagcolor == "blue";
                    break;
                case 1:
                    tagcolor == "red";
                    break;
                case 2:
                    tagcolor == "white";
                    break;
                default:
                    tagcolor == "blue";
            }

            tag.Open();

            std::string jsonString;
            jsonString = MQTTJson::CreateLiveDataJSON(tag.ReadMessage(),tag.GetUID(),tagcolor);
            mqttModule.GetTopics().at("live")->publish(jsonString);

            parser.ParseAndExecute(instrQueue.front());
            instrQueue.pop_front();

            char *uid = freefare_get_tag_uid(tag.GetFreefareTag());
            auto time = std::time(nullptr);
            auto putTime = std::put_time(std::localtime(&time), "%c");
            std::stringstream stream;
            stream << putTime;
            inv.push_back(InventoryEntry{stream.str(), std::vector<uint8_t>(uid, uid + strlen(uid)), tagcolor});

            color++;
            color = color % 3;

        }
        mqttModule.Quit();
    }

    static void NFCWrite(NFC::NFCInstructionSet instr) {

        if (!instr.All.GetMessage().empty()) {
            tag.WriteMessage(instr.All);
        }

        if(tagcolor == "blue") {
            if (!instr.Blue.GetMessage().empty()) {
                tag.WriteMessage(instr.Blue);
            }
        }

        if(tagcolor == "red") {
            if (!instr.Red.GetMessage().empty()) {
                tag.WriteMessage(instr.Red);
            }
        }

        if(tagcolor == "white") {
            if (!instr.White.GetMessage().empty()) {
                tag.WriteMessage(instr.White);
            }
        }

        for (auto elem : instr.UID) {
            if (tag.GetUID() == elem.first) {
                tag.WriteMessage(elem.second);
            }
        }


    }

    static void NFCAppend(NFC::NFCInstructionSet instr) {

        if (!instr.All.GetMessage().empty()) {
            NFC::NFCMessage msg = tag.ReadMessage();
            msg.AppendToMessage(instr.All.GetMessage());
            tag.WriteMessage(msg);
        }

        if(tagcolor == "blue") {
            if (!instr.Blue.GetMessage().empty()) {
                NFC::NFCMessage msg = tag.ReadMessage();
                msg.AppendToMessage(instr.Blue.GetMessage());
                tag.WriteMessage(msg);
            }
        }

        if(tagcolor == "red") {
            if (!instr.Red.GetMessage().empty()) {
                NFC::NFCMessage msg = tag.ReadMessage();
                msg.AppendToMessage(instr.Red.GetMessage());
                tag.WriteMessage(msg);
            }
        }

        if(tagcolor == "white") {

            if (!instr.White.GetMessage().empty()) {
                NFC::NFCMessage msg = tag.ReadMessage();
                msg.AppendToMessage(instr.White.GetMessage());
                tag.WriteMessage(msg);
            }
        }

        for (auto elem : instr.UID) {
            if (tag.GetUID() == elem.first) {
                NFC::NFCMessage msg = tag.ReadMessage();
                msg.AppendToMessage(elem.second.GetMessage());
                tag.WriteMessage(msg);
            }
        }

    }

    static void StartProg() {
        LOG("Start");
    }

    static void StopProg() {
        LOG("Stop");
    }

private:
    static void PrintVector(std::vector<uint8_t> vec) {
        PrintHex(vec.data(), vec.size());
        PrintAscii(reinterpret_cast<char *>(vec.data()), vec.size());
    }
};


int main() {
    if(__argc > 1) {
        MAINTest().Start(__argv[0]);
    }
    std::cout<<"your forgot the address..."<<std::endl;
}

