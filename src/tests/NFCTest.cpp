//
// Created by Rylox on 25.06.2018.
//

#include "NFCTest.h"

#include "../mqtt/InstructionParser.h"
#include "../mqtt/MQTTJson.h"

enum class TagColor{
    BLUE,
    RED,
    WHITE
};

class NFCTest {
public:
    NFCTest() {
    }

    void Start() {
            NFC::NFCModule module;
        for (int i = 0; i < 3; i++) {
            LOG("Polling for new Tag...");
            auto tag = module.Next();
            LOG("Found one!");
            tag.Open();
            LOG("[UID] :");
            PrintVector(tag.GetUID());
            LOG("[Current Tag Message]: ");
            auto msg = tag.ReadMessage();
            PrintVector(msg.GetMessage(NFC::NFCMessageFormat::RAW));
            LOG("[Format]:");

            std::string format;
            auto mformat = msg.GetFormat();

            if (mformat == NFC::NFCMessageFormat::RAW) {
                format = "Raw";
            } else if (mformat == NFC::NFCMessageFormat::TEXT) {
                format = "Text";
            } else if (mformat == NFC::NFCMessageFormat::LINK) {
                format = "Link";
            } else {
                format = "Text";
            }
            LOG(format);
            LOG("[Clearing]");
            tag.Clear();
            LOG("[Cleared]");
            LOG("[Writing]");
            auto writeMsg = NFC::NFCMessage("HSS Freudenstadt laesst gruessen");
            writeMsg.SetFormat(NFC::NFCMessageFormat::TEXT);
            tag.WriteMessage(writeMsg);
            LOG("[Done]");

            LOG("[New Message]");
            PrintVector(tag.ReadMessage().GetMessage(NFC::NFCMessageFormat::RAW));
        }

    }

private:
    static void PrintVector(std::vector<uint8_t> vec) {
        PrintHex(vec.data(), vec.size());
        PrintAscii(reinterpret_cast<char *>(vec.data()), vec.size());
    }
};


int main() {
        NFCTest().Start();
}

