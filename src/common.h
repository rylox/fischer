//
// Created by genkinger on 15/06/18.
//

#ifndef FISCHER_COMMON_H
#define FISCHER_COMMON_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <error.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <iostream>
#include <sstream>

#define ARRAY_SIZE(x) (sizeof(x)/sizeof((x)[0]))
#define LOG(x) std::cout << (x) << std::endl
#define ERR(x,code) std::cerr << (x) << std::endl;exit(code)

void PrintHex(const uint8_t *pbtData, const size_t szBytes);
void PrintAscii(const char *pbtData, const size_t szBytes);
std::string timestr(time_t t);

#endif //FISCHER_COMMON_H
