//
// Created by genkinger on 01/07/18.
//

#include "InstructionParser.h"

void InstructionParser::RegisterWriteFunction(std::function<void(NFC::NFCInstructionSet)> func) {
    mWriteFunction = func;
}

void InstructionParser::RegisterAppendFunction(std::function<void(NFC::NFCInstructionSet)> func) {
    mAppendFunction = func;
}

void InstructionParser::RegisterStopFunction(std::function<void()> func) {
    mStopFunction = func;
}

void InstructionParser::RegisterStartFunction(std::function<void()> func) {
    mStartFunction = func;
}

void InstructionParser::ParseAndExecute(std::string jsonString) {
    using json = nlohmann::json;
    auto obj = json::parse(jsonString);

    std::string instruction = obj["instruction"].get<std::string>();
    std::transform(std::begin(instruction), std::end(instruction), std::begin(instruction), ::tolower);

    auto data = obj["data"];

    auto all = data["all"];
    auto blue = data["blue"];
    auto red = data["red"];
    auto white = data["white"];

    NFC::NFCInstructionSet instructionSet;
    instructionSet.All = NFC::NFCMessage(all["data"].get<std::string>());
    instructionSet.All.SetFormat(GetFormatFromString(all["format"].get<std::string>()));

    instructionSet.Blue = NFC::NFCMessage(blue["data"].get<std::string>());
    instructionSet.Blue.SetFormat(GetFormatFromString(blue["format"].get<std::string>()));

    instructionSet.Red = NFC::NFCMessage(red["data"].get<std::string>());
    instructionSet.Red.SetFormat(GetFormatFromString(red["format"].get<std::string>()));

    instructionSet.White = NFC::NFCMessage(white["data"].get<std::string>());
    instructionSet.White.SetFormat(GetFormatFromString(white["format"].get<std::string>()));

    auto uids = data["uid"].get<std::vector<json>>();
    for (auto elem : uids) {
        auto data = elem["data"];
        auto msg = NFC::NFCMessage(data["data"].get<std::string>());
        msg.SetFormat(GetFormatFromString(data["format"].get<std::string>()));
        instructionSet.UID[elem["id"].get<std::vector<uint8_t>>()] = msg;
    }

    if (instruction == "write") {
        mWriteFunction(instructionSet);
    } else if (instruction == "append") {
        mAppendFunction(instructionSet);
    } else if (instruction == "start") {
        mStartFunction();
    } else if (instruction == "stop") {
        mStopFunction();
    } else {
        LOG("Malformed json string...");
    }


}

NFC::NFCMessageFormat InstructionParser::GetFormatFromString(std::string str) {
    std::transform(std::begin(str),std::end(str),std::begin(str),::tolower);
    if(str == "raw"){
        return NFC::NFCMessageFormat::RAW;
    }
    if(str == "link"){
        return NFC::NFCMessageFormat::LINK;
    }
    if(str == "text"){
        return NFC::NFCMessageFormat::TEXT;
    }
    if(str == "machine"){
        return NFC::NFCMessageFormat::MACHINE;
    }
    else{
        return NFC::NFCMessageFormat::RAW;
    }
}
