//
// Created by Rylox on 25.06.2018.
//

#include "MQTTModule.h"

#define CONNECT_TIMEOUT 5
#define KEEP_ALIVE 5
#define QOS 1

namespace MQTT {

    MQTTModule::Callback::Callback(MQTTModule *test) : mRef(test) {}

    void MQTTModule::Callback::connected(const std::string &cause) {
        LOG("Connected to server...");
        mRef->mTopics["connection"]->publish("1")->wait();
        mRef->mClient.subscribe("sensor/status/instructions/" + mRef->mClient.get_client_id(),QOS)->wait();
    }

    void MQTTModule::Callback::connection_lost(const std::string &cause) {
        LOG("Lost connection...");
        LOG(cause);
    }

    void MQTTModule::Callback::message_arrived(mqtt::const_message_ptr msg) {
        LOG("Message arrived");
        std::cout << "Topic: " << msg->get_topic() << std::endl;
        LOG(msg->get_payload_str());
    }

    void MQTTModule::Callback::delivery_complete(mqtt::const_delivery_token_ptr tok) {
        LOG("Delivery complete...");
        LOG(tok->get_message()->get_payload_str());
    }

    void MQTTModule::ActionListener::on_failure(const mqtt::token &asyncActionToken) {};

    void MQTTModule::ActionListener::on_success(const mqtt::token &asyncActionToken) {};

    MQTTModule::MQTTModule(const std::string address, const std::string id) : mClient(address, id), mCallback(this),
                                                                              mConOpts() {

        mClient.set_callback(mCallback);

        mConOpts.set_keep_alive_interval(KEEP_ALIVE);
        mConOpts.set_clean_session(true);
        mConOpts.set_automatic_reconnect(true);
        mConOpts.set_connect_timeout(CONNECT_TIMEOUT);

        mTopics["connection"] = mqtt::topic::create(mClient, "sensors/status/connection/" + id, QOS);
        mTopics["run"] = mqtt::topic::create(mClient, "sensors/status/run/" + id, QOS);
        mTopics["inventory"] = mqtt::topic::create(mClient, "sensors/status/inventory/" + id, QOS);
        mTopics["instructions"] = mqtt::topic::create(mClient, "sensors/status/instructions/" + id, QOS);
        mTopics["live"] = mqtt::topic::create(mClient, "sensors/status/live/" + id, QOS);

        auto will = mqtt::will_options(*mTopics["connection"], "0", 1, 1, false);
        mConOpts.set_will(will);
    }

    const std::map<std::string, mqtt::topic_ptr> &MQTTModule::GetTopics() {
        return mTopics;
    };

    void MQTTModule::Connect() {
        //Connect & Subscribe on Success in Callback
        mClient.connect(mConOpts)->wait();

    }

    void MQTTModule::Quit() {
        //Disconnect
        mTopics["connection"]->publish("0")->wait();
        mClient.disconnect()->wait();
    }

    mqtt::async_client &MQTTModule::GetClient() {
        return mClient;
    }

}