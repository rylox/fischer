//
// Created by Rylox on 01.07.2018.
//

#ifndef FISCHER_MQTTJSON_H
#define FISCHER_MQTTJSON_H

#include "../dependencies/json.hpp"
#include "../nfc/NFCMessage.h"
#include <string>

class MQTTJson {

public:
    static std::string CreateLiveDataJSON(NFC::NFCMessage nfcmsg, std::vector<uint8_t> uid, std::string color);
};

#endif //FISCHER_MQTTJSON_H
