//
// Created by Rylox on 25.06.2018.
//

#ifndef FISCHER_MQTT_COM_H
#define FISCHER_MQTT_COM_H

#include "mqtt/async_client.h"
#include "mqtt/delivery_token.h"
#include "mqtt/message.h"
#include "mqtt/exception.h"
#include "../common.h"
#include "../mqtt/"

#include <map>

namespace MQTT {
    class MQTTModule {

    private:

        class Callback : public virtual mqtt::callback {
        public:
            Callback(MQTTModule *test) : mRef(test) {}

            void connected(const std::string &cause) override;

            void connection_lost(const std::string &cause) override;

            void message_arrived(mqtt::const_message_ptr msg) override;

            void delivery_complete(mqtt::const_delivery_token_ptr tok) override;

        private:
            MQTTModule *mRef;
        };

        class ActionListener : public virtual mqtt::iaction_listener {
            void on_failure(const mqtt::token &asyncActionToken);

            void on_success(const mqtt::token &asyncActionToken);
        };

    public:
        MQTTModule(const std::string address, const std::string id);
        void Connect();
        void Quit();

        const std::map<std::string, mqtt::topic_ptr> &GetTopics();
        mqtt::async_client& GetClient();

    private:
        Callback mCallback;
        mqtt::async_client mClient;
        mqtt::connect_options mConOpts;
        std::map<std::string, mqtt::topic_ptr> mTopics;
    };


}

#endif //FISCHER_MQTT_COM_H
