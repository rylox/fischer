//
// Created by genkinger on 01/07/18.
//

#ifndef FISCHER_INSTRUCTIONPARSER_H
#define FISCHER_INSTRUCTIONPARSER_H

#include <functional>
#include <algorithm>
#include "../nfc/NFCInstructionSet.h"
#include "../dependencies/json.hpp"


class InstructionParser {

public:
    void RegisterWriteFunction(std::function<void(NFC::NFCInstructionSet)> func);
    void RegisterAppendFunction(std::function<void(NFC::NFCInstructionSet)> func);
    void RegisterStopFunction(std::function<void()> func);
    void RegisterStartFunction(std::function<void()> func);


    void ParseAndExecute(std::string jsonString);
    
private:
    NFC::NFCMessageFormat GetFormatFromString(std::string str);
private:
    
    std::function<void(NFC::NFCInstructionSet)> mWriteFunction;
    std::function<void(NFC::NFCInstructionSet)> mAppendFunction;
    std::function<void()> mStopFunction;
    std::function<void()> mStartFunction;

};


#endif //FISCHER_INSTRUCTIONPARSER_H
