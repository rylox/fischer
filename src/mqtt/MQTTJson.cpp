//
// Created by Rylox on 01.07.2018.
//

#include <chrono>
#include "MQTTJson.h"

static std::string MQTTJson::CreateLiveDataJSON(NFC::NFCMessage nfcmsg, std::vector<uint8_t> uid, std::string color) {
    using json = nlohmann::json;

    auto msg = nfcmsg.GetMessage();

    std::time_t t = std::time(nullptr);

    json j;
    j["color"] = color;
    j["uid"] = uid;
    j["msg"] = std::string(msg.begin(), msg.end());
    j["format"] = nfcmsg.GetFormat();
    j["timestamp"] = timestr(t);

    std::string jstring = j.dump();
    return jstring;
}
